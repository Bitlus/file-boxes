# File Boxes

An application for organizing CSV files into boxes.

## Documentation
Documentation for this project can be found [here](https://gitlab.com/Bitlus/file-boxes/-/wikis/home) in the wiki of this project.
