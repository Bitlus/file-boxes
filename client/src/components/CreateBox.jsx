import axios from "axios";
import React from "react";
import { useState } from "react";
import { Button, Form, Header } from "semantic-ui-react";
import { Redirect } from "react-router-dom";

/**
 * Form for creating a new box.
 * Redirects to box route when the box is created.
 */
function CreateBox() {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [newBoxId, setNewBoxId] = useState("");

  async function createBox() {
    setIsLoading(true);
    const response = await axios.post(
      "https://file-boxes-server-3kubf3abuq-ts.a.run.app/boxes/",
      {
        name: name,
        description: description,
      }
    );

    const newBox = response.data;
    setNewBoxId(newBox._id);
  }

  if (newBoxId) return <Redirect to={`/boxes/${newBoxId}`} />;
  else
    return (
      <div>
        <Header as="h2">Create New Box</Header>
        <Form>
          <Form.Field>
            <label>Box Name:</label>
            <input
              disabled={isLoading}
              onChange={(e) => setName(e.target.value)}
              placeholder="name"
            />
          </Form.Field>
          <Form.Field>
            <label>Box Description:</label>
            <input
              disabled={isLoading}
              onChange={(e) => setDescription(e.target.value)}
              placeholder="description"
            />
          </Form.Field>
          <Button
            primary
            loading={isLoading}
            disabled={!name || !description || isLoading}
            onClick={createBox}
          >
            Create Box
          </Button>
        </Form>
      </div>
    );
}

export default CreateBox;
