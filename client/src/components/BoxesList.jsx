import React from "react";
import { useState, useEffect } from "react";
import { Item, Loader } from "semantic-ui-react";
import axios from "axios";
import BoxItem from "./BoxItem";

/**
 * Gets box diplay and displays is as a list
 *
 * @property {boolean} isLoading whether the component is loading data or not
 * @property {object[]} boxData an array of boxes from the box API
 */
function BoxesList() {
  const [isLoading, setIsLoading] = useState(true);
  const [boxData, setBoxData] = useState([]);

  // Get data from API
  useEffect(async () => {
    // TODO: Implement a way to get base url based on dev/prod enviroment
    const response = await axios.get(
      "https://file-boxes-server-3kubf3abuq-ts.a.run.app/boxes"
    );
    setBoxData(response.data);
    setIsLoading(false);
  }, []);

  if (isLoading) return <Loader active>Loading Boxes</Loader>;
  else
    return (
      <Item.Group>
        {boxData.map((box) => (
          <BoxItem key={box._id} box={box} />
        ))}
      </Item.Group>
    );
}

export default BoxesList;
