import React from "react";
import { Header } from "semantic-ui-react";
import BoxesList from "./BoxesList";

function BoxesIndex() {
  return (
    <div>
      <Header as="h2">Boxes</Header>
      <BoxesList />
    </div>
  );
}

export default BoxesIndex;
