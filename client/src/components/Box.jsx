import React from "react";
import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import { Header, Item, Loader } from "semantic-ui-react";

/**
 * Gets and displays a boxes data
 */
function Box() {
  const [isLoading, setIsLoading] = useState(true);
  const [box, setBox] = useState({});

  let { id } = useParams(); // Get id from react router

  // Get box data
  useEffect(async () => {
    const response = await axios.get(
      `https://file-boxes-server-3kubf3abuq-ts.a.run.app/boxes/${id}`
    );
    const box = response.data;
    setBox(box);
    setIsLoading(false);
  }, []);

  if (isLoading) return <Loader active>Loading Box</Loader>;
  else
    return (
      <div>
        <Header as="h2">Box</Header>
        <Link to={`/boxes/${id}/edit`}>Edit Box</Link>
        <Item>
          <Item.Content>
            <Item.Header>
              <Header as="h3">{box.name}</Header>
            </Item.Header>
            <Item.Meta>{box.description}</Item.Meta>
          </Item.Content>
        </Item>
      </div>
    );
}

export default Box;
