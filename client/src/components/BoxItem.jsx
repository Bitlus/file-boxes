import React from "react";
import { Link } from "react-router-dom";
import { Icon, Item } from "semantic-ui-react";

/**
 * Displays a boxes name and links to said box.
 *
 * @param {object} props.box a box object
 */
function BoxItem(props) {
  return (
    <Item>
      <Item.Content>
        <Item.Header>
          <Link to={`/boxes/${props.box._id}`}>{props.box.name}</Link>
        </Item.Header>
        <Item.Meta>{props.box.description}</Item.Meta>
      </Item.Content>
    </Item>
  );
}

export default BoxItem;
