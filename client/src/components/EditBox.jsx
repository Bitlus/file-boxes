import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import { Button, Form, Header } from "semantic-ui-react";
import { Link, Redirect, useParams } from "react-router-dom";

/**
 * Form for editing a box.
 * Redirects to box route when the box is updated.
 */
function EditBox() {
  const [isLoading, setIsLoading] = useState(true);
  const [currentName, setCurrentName] = useState("");
  const [currentDescription, setCurrentDescription] = useState("");
  const [newName, setNewName] = useState("");
  const [newDescription, setNewDescription] = useState("");
  const [updatedBoxId, setUpdatedBoxId] = useState("");

  const { id } = useParams();
  const boxUrl = `https://file-boxes-server-3kubf3abuq-ts.a.run.app/boxes/${id}`;

  // Get box data
  useEffect(async () => {
    const response = await axios.get(boxUrl);
    const box = response.data;

    setCurrentName(box.name);
    setCurrentDescription(box.description);
    setNewName(box.name);
    setNewDescription(box.description);

    setIsLoading(false);
  }, []);

  async function editBox() {
    setIsLoading(true);

    let updatedBoxDetails = {};
    if (newName !== currentName) updatedBoxDetails.name = newName;
    if (newDescription !== currentDescription)
      updatedBoxDetails.description = newDescription;

    const response = await axios.patch(boxUrl, updatedBoxDetails);

    const updatedBox = response.data;
    setUpdatedBoxId(updatedBox._id);
  }

  /**
   * Checks whether new values are same as old.
   * @returns {boolean} true if new values are same as current false if not
   */
  function newSameAsCurrent() {
    return newName === currentName && newDescription === currentDescription;
  }

  if (updatedBoxId) return <Redirect to={`/boxes/${updatedBoxId}`} />;
  else
    return (
      <div>
        <Link to={`/boxes/${id}`}>Go back to box.</Link>
        <Header as="h2">Edit Box</Header>
        <Form>
          <Form.Field>
            <label>Box Name:</label>
            <input
              disabled={isLoading}
              onChange={(e) => setNewName(e.target.value)}
              placeholder="name"
              value={newName}
            />
          </Form.Field>
          <Form.Field>
            <label>Box Description:</label>
            <input
              disabled={isLoading}
              onChange={(e) => setNewDescription(e.target.value)}
              placeholder="description"
              value={newDescription}
            />
          </Form.Field>
          <Button
            primary
            loading={isLoading}
            disabled={
              !newName || !newDescription || newSameAsCurrent() || isLoading
            }
            onClick={editBox}
          >
            Update Box
          </Button>
        </Form>
      </div>
    );
}

export default EditBox;
