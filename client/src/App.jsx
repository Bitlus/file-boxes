import React from "react";
import {
  BrowserRouter as Router,
  Link,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { Container, Header } from "semantic-ui-react";
import BoxesIndex from "./components/BoxesIndex";
import Box from "./components/Box";
import CreateBox from "./components/CreateBox";
import EditBox from "./components/EditBox";

/**
 * The root element of the application.
 * Handles routing/navitgation to components.
 */
function App() {
  return (
    <Router>
      <Header as="h1">File Boxes</Header>
      <nav>
        <ul>
          <li>
            <Link to="/boxes">Boxes</Link>
          </li>
          <li>
            <Link to="/boxes/create">Create a New Box</Link>
          </li>
        </ul>
      </nav>
      <Container>
        <Switch>
          <Route path="/boxes/create">
            <CreateBox />
          </Route>
          <Route path="/boxes/:id/edit" children={<EditBox />} />
          <Route path="/boxes/:id" children={<Box />} />
          <Route path="/boxes">
            <BoxesIndex />
          </Route>
          <Route path="/">
            <Redirect to="/boxes" />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
