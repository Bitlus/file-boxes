import Box from "../models/box.js";

/**
 * Retrieves boxes from the database based on the filter object.
 *
 * @param {object} filter a Mongoose filter object
 * @returns {object[]} an array of boxes
 */
export async function getBoxes(filter) {
  const boxes = await Box.find(filter);
  return boxes;
}

/**
 * Retrieves a box from the database by id.
 *
 * @param {string} id the id of the box
 * @returns {object} the box if found null if not
 */
export async function getBoxById(id) {
  const box = await Box.findById(id).exec();
  return box;
}

/**
 * Creates a box in the database.
 *
 * @param {string} name
 * @param {string} description
 * @returns {object} the created box
 */
export async function createBox(name, description) {
  const box = new Box({ name: name, description: description });
  await box.save();

  return box;
}

/**
 * Updates a box in the database.
 *
 * @param {string} id the id of the box
 * @param {object} update an object containing the updated fields of the box
 * @returns {object} the updated box
 */
export async function updateBox(id, update) {
  const updatedBox = await Box.findOneAndUpdate({ _id: id }, update, {
    new: true,
  });
  return updatedBox;
}
