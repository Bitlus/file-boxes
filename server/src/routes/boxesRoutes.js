import express from "express";
import {
  createBox,
  getBoxById,
  getBoxes,
  updateBox,
} from "../services/boxesService.js";

const boxesRouter = express.Router();

/**
 * Gets all boxes from the database.
 */
boxesRouter.get("/", async (req, res) => {
  try {
    const boxes = await getBoxes({});
    res.json(boxes);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

/**
 * Gets a specific box based on the id given in the URL.
 */
boxesRouter.get("/:boxId", async (req, res) => {
  const boxId = req.params.boxId;

  try {
    const box = await getBoxById(boxId);

    if (box) res.json(box);
    else res.sendStatus(404);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

/**
 * Creates a new box.
 * Expects a name and description in the body of a JSON request.
 */
boxesRouter.post("/", async (req, res) => {
  const name = req.body.name;
  const description = req.body.description;

  // If there is no name and/or description than send bad request
  if (!name || !description) return res.status(400).end();

  try {
    const box = await createBox(name, description);
    res.status(201).json(box);
  } catch (err) {
    console.err(err);
    res.status(500);
  }
});

/**
 * Updates an existing box based on the id given in the URL.
 * Expects a name and/or decription in the body of a JSON request.
 */
boxesRouter.patch("/:boxId", async (req, res) => {
  const boxId = req.params.boxId;
  const name = req.body.name;
  const description = req.body.description;

  // Construct an update object to only include the fields sent
  let update = {};
  if (name) update.name = name;
  if (description) update.description = description;

  // If the update object is empty it won't update anything therefore bad request
  if (Object.keys(update).length === 0) return res.sendStatus(400);

  try {
    const box = await updateBox(boxId, update);
    res.status(200).json(box);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

export default boxesRouter;
