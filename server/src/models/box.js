import mongoose from "mongoose";

const { Schema } = mongoose;

const boxSchema = new Schema(
  {
    name: String,
    description: String,
    csvFileUrl: String,
  },
  { timestamps: true }
);

const Box = mongoose.model("Box", boxSchema);

export default Box;
