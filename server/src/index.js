import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import boxesRouter from "./routes/boxesRoutes.js";

// Gets enviroment variables from .env file
dotenv.config();
const port = process.env.PORT;
const mongodbConnectionString = process.env.MONGODB_CONNECTION_STRING;

// Configure Mongoose
await mongoose.connect(mongodbConnectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

// Configure Express
const app = express();
app.use(express.json());
app.use("/boxes", boxesRouter);

// Start Express server
app.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);
});
